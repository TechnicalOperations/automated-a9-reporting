#!/usr/bin/python

import imaplib
import smtplib
import email
import os
import sys
import re
import csv
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
import email.encoders as Encoders
from datetime import datetime
from datetime import timedelta
import time


def mail_check():
    def csv_write(csv_encoded_arr):
        print("[+]Saving CSV to temp file")
        csvfile = open(fn_create(), 'w')
        for i in csv_encoded_arr:
            csvfile.write("{}\n".format(i))
        csvfile.close()
        return

    def delete_email(msgs):
        print("[+]Deleting report emails")
        for i in msgs:
            mail.store(i, '+FLAGS', '\\Deleted')
        mail.expunge()

    def attach_check(msgs):
        for i in msgs:
            resp, d = mail.fetch(i, "(RFC822)")
            body = d[0][1]
            res = email.message_from_string(str(body))

            for p in res.walk():
                filename = ""
                csv_encoded_arr = []
                m = re.search("filename=\"(.+.csv)\"", str(body))
                if m:
                    filename = str(m.group(1))
                    print("[+]Pulling attachment: {}".format(filename))
                m1 = re.search('X-Microsoft-Exchange-Diagnostics:\\\\r\\\\n\\\\t.+\\\\r\\\\n\\\\r\\\\n(.+)\\\\r\\\\n\\\\r\\\\n', str(body))
                if m1:
                    enc_str = m1.group(1)
                    enc_arr = enc_str.split("\\r\\n")
                    for i in enc_arr:
                        csv_encoded_arr.append(i)
                csv_write(csv_encoded_arr)
                return

    try:
        mail = imaplib.IMAP4_SSL('outlook.office365.com')
        mail.login('tops.report@rhythmone.com', '3QCZbUmqYAF6mJC8FEPd')
    except Exception as e:
        print(e)

    mail.list()
    mail.select("inbox")

    result, data = mail.search(None, '(SUBJECT "A9 Daily (yesterday)")')
    msgs = data[0].split()
    if len(msgs) < 1:
        print("[!]No email found. Quiting.")
        sys.exit()

    attach_check(msgs)
    delete_email(msgs)

def fn_create():
    year = time.strftime("%Y")
    month = time.strftime("%m")
    day = time.strftime("%d")
    s = month + day + year
    today = datetime.strptime(s, '%m%d%Y')
    offset = timedelta(days=1)
    y = today - offset
    fn = "R1A9 Automated Reporting {0}{1}{2}.csv".format(y.month,y.day,y.year)
    return fn


def mod_date(r):
    dt = datetime.strptime(str(r), '%d %b %Y')
    offset = timedelta(days=1)
    yesterday = dt - offset
    return [dt, yesterday]


def csv_corrector():
    print("[+]Modifying CSV")

    new_arr = []

    with open(fn_create(), 'r') as f:
        csv_read = csv.reader(f)
        rows = list(csv_read)[7:]
        for i in rows[:-1]:
            if i[0] == "Day":
                i[0] = "date"
            if i[0] != "date":
                dt,yesterday = mod_date(i[0])
                i[0] = "{0}/{1}/{2}".format(dt.day, dt.month, dt.year)
                i[0] = "{} EST".format(i[0])
            if i[2] == "Publisher":
                i[2] = "account_no"
            if i[5] == "Publisher Commission (CPx)":
                i[5] = "Spend"
            i[3] = i[3].replace(",","")

            m = re.search(".+\((\d+)\)", str(i))
            if m:
                i[2] = str(m.group(1))
            new_arr.append(i)

    with open(fn_create(), 'w') as f:
        csv_write = csv.writer(f)
        for i in new_arr:
            csv_write.writerow([i[0],i[1],i[2],i[3],i[4],i[5]])


def send_email():
    print("[+]Sending updated report")

    SUBJECT = "Amazon A9 - Automated Report"
    FROM = "tops.report@rhythmone.com"
    TO = "aps-tam-reports@amazon.com"
    CC = 'tops@rhythmone.com'

    msg = MIMEMultipart()
    msg['Subject'] = SUBJECT
    msg['From'] = FROM
    msg['To'] = TO
    msg['Cc'] = CC


    part = MIMEBase('application', "octet-stream")
    part.set_payload(open(fn_create(), "rb").read())
    Encoders.encode_base64(part)

    part.add_header('Content-Disposition', 'attachment; filename="{}"'.format(fn_create()))
    msg.attach(part)

    smtp = smtplib.SMTP('outlook.office365.com', 587)
    smtp.ehlo()
    smtp.starttls()
    smtp.ehlo
    smtp.login('tops.report@rhythmone.com', '3QCZbUmqYAF6mJC8FEPd')
    smtp.sendmail(FROM, [TO,CC], str(msg))

if __name__ == '__main__':
    mail_check()
    csv_corrector()
    send_email()
    os.remove(fn_create())
